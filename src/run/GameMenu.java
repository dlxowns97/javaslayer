package run;

import java.util.Scanner;

import screen.Field;

public class GameMenu {

	public static void main(String[] args) {
		
		GameMenu gm = new GameMenu();
		Field field = new Field();
		
		Scanner sc = new Scanner(System.in);
		
		do {
			System.out.println("==================================");
			System.out.println();
			System.out.println("            JavaSlayer            ");
			System.out.println();
			System.out.println("==================================");
			System.out.println("============== 메뉴창 ==============");
			System.out.println("1. 게임 시작");
			System.out.println("2. 게임 종료");
			System.out.print("메뉴 선택 : ");
			int num = sc.nextInt();

			switch (num) {
				case 1:
					System.out.println("게임을 시작합니다.");
					System.out.println();
					field.Screen();
					break;
				case 2:
					gm.stop();
					return;
				default:
					System.out.println("없는 메뉴입니다.");
					break;
			}

		} while(true);

	}
	
	public void stop() {
		System.out.println("게임을 종료합니다.");
	}

}
