package screen;

import java.util.Scanner;

import units.Monster;
import units.User;

public class Field {
	
//	Fight fight = new Fight();
	
	Scanner sc = new Scanner(System.in);
	
	public void Screen() {
		System.out.print("이름을 작성해주세요 : ");
		String userName = sc.next();
		System.out.println();
		
		int atk = 0;
		int def = 0;
		int hp = 0;
		int mp = 0;
		int lv = 1;
		int luk = 0;
		int avgAtk = 0;
		int exp = 0;
		int totalExp = 0;
		boolean isAlive = true;
		String job = "";
		
		while(true) {
			System.out.println("직업을 고르시오 : 검사(1) 마법사(2)");
			int jobNum = sc.nextInt();
			
			if(jobNum == 1) {
				atk = 20 + ((lv - 1) * 5);
				def = 20 + ((lv - 1) * 2);
				hp = 200 + ((lv - 1) * 10);
				mp = 50 + ((lv - 1) * 5);
				luk = 1 + (lv / 10);
				avgAtk = ((atk + def) / 10 + ((hp + mp) / 100) + luk);
				exp = 0;
				totalExp = 100 * lv;
				job = "검사";
				break;
			}
			else if(jobNum == 2) {
				atk = 10 + ((lv - 1) * 2);
				def = 0 + ((lv - 1) * 1);
				hp = 100 + ((lv - 1) * 5);
				mp = 300 + ((lv - 1) * 15);
				luk = 5 + (lv / 10);
				avgAtk = ((atk + def) / 10 + ((hp + mp) / 100) + luk);
				exp = 0;
				totalExp = 100 * lv;
				job = "마법사";
				break;
			}
			else {
				System.out.println("잘못 고르셨습니다. 직업을 다시 골라주십시오.\n");
			}
		}
		
		User player = new User(userName, atk, def, hp, mp, luk, lv, exp, totalExp, job, isAlive);
		
		String m_name[] = {"Method", "Array", "Object"};
		int num = (int)(Math.random() * m_name.length);
		int m_lv = (int)(Math.random() * 10) + 1;
		int m_atk = (int)((Math.random() * 20) + 1 + (m_lv * 3));
		int m_def = (int)((Math.random() * 10) + 1 + (m_lv * 1));
		String chapter[] = {"메소드", "배열", "객체"};
		int m_luk;
		int m_hp;
		int m_mp;
		int m_exp;
		int m_avgAtk;
		
		if(m_name[num] == "Method") {
			m_luk = (int)(Math.random() + (m_lv / 5));
			m_hp = (int)(Math.random() * 200 + (m_lv * 10));
			m_mp = 50 + (m_lv * 5);
			m_exp = 100 * m_lv;
			m_avgAtk = ((m_atk + m_def) / 10 + ((m_hp + m_mp) / 100) + m_luk);
		}
		else if(m_name[num] == "Array") {
			m_luk = (int)((Math.random() * 3) + (m_lv / 5));
			m_hp = (int)(Math.random() * 50 + (m_lv * 8));
			m_mp = 100 + (m_lv * 5);
			m_exp = 100 * m_lv;
			m_avgAtk = ((m_atk + m_def) / 10 + ((m_hp + m_mp) / 100) + m_luk);
		}
		else {
			m_luk = (int)((Math.random() * 5) + (m_lv / 5));
			m_hp = (int)(Math.random() * 100 + (m_lv * 8));
			m_mp = 0;
			m_exp = 100 * m_lv;
			m_avgAtk = ((m_atk + m_def) / 10 + ((m_hp + m_mp) / 100) + m_luk);
		}
		
		Monster m = new Monster(m_name[num], m_atk, m_def, m_hp, 0, m_luk, m_lv, m_exp, chapter[num], isAlive);
		
//		System.out.print(m_avgAtk);
		
//		while(isAlive)
		System.out.println("===========Stats===========");
		player.Stats();
		System.out.println("===========================\n");
		System.out.println("몬스터와 조우하셨습니다.");
		
		while(true) {
			System.out.println("싸우시겠습니까? : Y(1) / N(2)");
			int select = sc.nextInt();
			
			if(select == 1) {
				System.out.println();
				System.out.println("전투를 시작합니다.");
				System.out.println("==========M_Stats==========");
				m.Stats();
				System.out.println("===========================\n");
				
//				while(isAlive) {
//					
//				}
				
				return;
			}
			else if(select == 2) {
				if(avgAtk > m_avgAtk) {
					System.out.println();
					System.out.println("전투에서 도망치셨습니다.");
					break;
				}
				else {
					System.out.println();
					System.out.println("도망치지 못했습니다.");
					System.out.println("전투를 시작합니다.");
					System.out.println("==========M_Stats==========");
					m.Stats();
					System.out.println("===========================\n");
					break;
				}
			}
		}
		
	}
	
}
