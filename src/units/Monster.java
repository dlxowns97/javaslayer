package units;

public class Monster extends Unit {

	private String chapter;
	
	public Monster(String name, int atk, int def, int hp, int mp, int luk, int lv, int exp, String chapter, boolean isAlive) {
		super(name, atk, def, hp, mp, luk, lv, exp, isAlive);
		this.chapter = chapter;
	}

	@Override
	public void Stats() {
		super.Stats();
		
		System.out.println("리젠 위치 : " + chapter);
	}
	
	@Override
	public void Cry() {
		System.out.println("이걸 이해하네?");
	}
	
}
