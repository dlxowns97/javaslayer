package units;

public abstract class Unit {

	private String name;
	private int atk;
	private int def;
	private int hp;
	private int mp;
	private int luk;
	private int lv;
	private int exp;
	private int dmg;
	private boolean isAlive;
	
	public Unit(String name, int atk, int def, int hp, int mp, int luk, int lv, int exp, boolean isAlive) {
		super();
		this.name = name;
		this.atk = atk;
		this.def = def;
		this.hp = hp;
		this.mp = mp;
		this.luk = luk;
		this.lv = lv;
		this.exp = exp;
		this.isAlive = true;
	}
	
	public void Stats() {
		System.out.printf("이름 : %6s\t레벨 : %6d\n", name, lv);
		System.out.printf("공격력 : %5d\t방어력 : %5d\n", atk, def);
		System.out.printf("체력 : %6d\t마력 : %6d\n", hp, mp);
		System.out.printf("행운 : %3d\t", luk);
		System.out.printf("보유경험치 : %2d\n",exp);
	}
	
	public boolean isAlive() {
		return isAlive;
	}
	
	public int getAtkDmg() {
		return atk;
	}
	
	public void lossHp(int amount) {
		if(hp >= amount) {
			this.hp -= amount;
			System.out.println("hp가 " + amount + "만큼 감소합니다.");
		} else {
			if(hp != 0) {
				System.out.println("hp가 " + hp + "만큼 감소합니다.");
			}
			
			hp = 0;
			isAlive = false;
			
			System.out.println("사망하셨습니다.");
		}
	}

	public abstract void Cry();
	
}
