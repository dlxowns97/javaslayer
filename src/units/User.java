package units;

public class User extends Unit {
	private String job;
	private int totalExp;
	
	public User(String name, int atk, int def, int hp, int mp, int luk, int lv, int exp, int totalExp, String job, boolean isAlive) {
		super(name, atk, def, hp, mp, luk, lv, exp, isAlive);
		this.job = job;
		this.totalExp = totalExp;
	}
	
	@Override
	public void Stats() {
		super.Stats();
		System.out.println("총경험치 : " + totalExp);
		System.out.println("직업 : " + job);
	}
	
	@Override
	public void Cry() {
		System.out.println("끄아아아악~!");
	}
}
